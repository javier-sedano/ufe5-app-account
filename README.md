Part of the microfrontend reference architecture described in https://gitlab.com/javier-sedano/ufe5

Read that documentation.

This is an app created in Vue3 for the /account/ URL and the Social Login callbacks.
