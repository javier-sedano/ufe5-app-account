// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  app: {
    baseURL: '/account/',
  },
  runtimeConfig: {
    public: {
      baseUrl: `http://localhost:${process.env.PORT}/`,
    },
  },
})
